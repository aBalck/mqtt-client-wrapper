package de.haw;

import java.util.List;

public interface Handler {
    /**
     * Gives a list with all topics, that should be subscribed to
     */
    public List<String> getTopics();

    /**
     * This method handles incoming messages and changes the corresponding fsm
     * 
     * @param message
     */
    public void handle(Message message);

    /**
     * Sets a wrapper
     * 
     * @param mqttWrapper
     */
    public void setMqttWrapper(MqttClientWrapper mqttWrapper);

    /**
     * Published a message over the corresponding mqttWrapper
     * 
     * @param message
     */
    public void publish(Message message);
}
