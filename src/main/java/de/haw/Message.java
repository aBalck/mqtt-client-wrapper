package de.haw;

import java.util.Objects;

/**
 * Class that represents a received MQTT-Message
 */
public class Message {
    private String payload;
    private String topic;

    public Message(String payload, String topic) {
        this.payload = payload;
        this.topic = topic;
    }

    public String getPayload() {
        return this.payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getTopic() {
        return this.topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Message)) {
            return false;
        }
        Message message = (Message) o;
        return Objects.equals(payload, message.payload) && Objects.equals(topic, message.topic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(payload, topic);
    }

    @Override
    public String toString() {
        return "{" + " payload='" + getPayload() + "'" + ", topic='" + getTopic() + "'" + "}";
    }

}
