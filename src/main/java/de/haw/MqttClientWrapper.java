package de.haw;

import java.time.LocalDateTime;

/**
 * Wrapps a handler(client) the handle method of the handler gets called, every
 * time a new message was received
 */
public class MqttClientWrapper {

    OwnMqttClient mqttClient;
    Thread t;

    /**
     * Starts a thread that will call the handle method of the given handler, every
     * time a new message arrives
     * 
     * @param handler    that should be called
     * @param clientName name of this client
     * @param brokerName name of the mqttBroker (e.g. 'localhost')
     * @param brokerPort port of the broker
     */
    public MqttClientWrapper(Handler handler, String clientName, String brokerName, int brokerPort) {
        mqttClient = new OwnMqttClient(clientName + "_" + LocalDateTime.now().toString(), brokerName, brokerPort);
        mqttClient.createBlockingClient();
        mqttClient.connectToBroker();

        for (String topic : handler.getTopics()) {
            mqttClient.subscribe(topic);
        }
        t = mqttClient.subscribeAndCall_threadLoop(handler);
        t.start();
    }

    /**
     * Starts a thread that will call the handle method of the given handler, every
     * time a new message arrives
     * 
     * @param handler    that should be called
     * @param clientName name of this client
     * @param brokerName name of the mqttBroker (e.g. 'localhost')
     */
    public MqttClientWrapper(Handler handler, String clientName, String brokerName) {
        this(handler, clientName, brokerName, 1883);
    }

    /**
     * Starts a thread that will call the handle method of the given handler, every
     * time a new message arrives from a local broker
     * 
     * @param handler    that should be called
     * @param clientName name of this client
     */
    public MqttClientWrapper(Handler handler, String clientName) {
        this(handler, clientName, "localhost");
    }

    /**
     * stops the thread that listens to new messages
     */
    public void stop() {
        if (mqttClient != null) {
            mqttClient.disconnect();
        }

        if (t != null) {
            t.interrupt();
        }
    }

    /**
     * publishes a new message
     * 
     * @param message
     */
    public void publish(Message message) {
        if (mqttClient != null)
            mqttClient.publish(message.getTopic(), message.getPayload());
    }
}
