package de.haw;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;

import com.hivemq.client.mqtt.MqttClient;
import com.hivemq.client.mqtt.MqttGlobalPublishFilter;
import com.hivemq.client.mqtt.datatypes.MqttQos;
import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient;
import com.hivemq.client.mqtt.mqtt5.Mqtt5BlockingClient.Mqtt5Publishes;
import com.hivemq.client.mqtt.mqtt5.message.publish.Mqtt5Publish;

public class OwnMqttClient {
    private String _broker = "localhost";
    private int _port = 1883;
    private String _clientName;
    private Mqtt5BlockingClient _client;
    private Mqtt5Publishes publishes;

    public OwnMqttClient(String clientName) {
        this._clientName = clientName;
    }

    public OwnMqttClient(String clientName, String brokerName) {
        this._clientName = clientName;
        this._broker = brokerName;
    }

    public OwnMqttClient(String clientName, String brokerName, int port) {
        this._clientName = clientName;
        this._broker = brokerName;
        this._port = port;
    }

    public void createBlockingClient() {
        _client = MqttClient.builder().identifier(_clientName).serverHost(_broker).serverPort(_port).useMqttVersion5()
                .buildBlocking();
    }

    public void connectToBroker() {
        _client.connect();
    }

    public void disconnect() {
        _client.disconnect();
    }

    public void subscribe(String topic) {
        if (publishes == null) {
            publishes = _client.publishes(MqttGlobalPublishFilter.SUBSCRIBED);
            _client.subscribeWith().topicFilter(topic).qos(MqttQos.AT_LEAST_ONCE).send();
        }
        _client.subscribeWith().addSubscription().topicFilter(topic).qos(MqttQos.AT_LEAST_ONCE).applySubscription()
                .send();
    }

    public Message getNextMessage_blocking() {
        if (publishes == null) {
            System.err.println("no topic subscripted");
        }
        try {
            Mqtt5Publish publishedMessage = publishes.receive();

            Charset charset = Charset.forName("ISO-8859-1");
            String payload;
            payload = charset.decode(publishedMessage.getPayload().get()).toString();
            String topic;
            topic = charset.decode(publishedMessage.getTopic().toByteBuffer()).toString();

            return new Message(payload, topic);
        } catch (Exception e) {
            System.err.println("getNextMessage_blocking: " + e.getMessage());
            return null;
        }
    }

    public Thread subscribeAndCall_threadLoop(Object obj, Method method) {
        Runnable r = () -> {
            while (!Thread.interrupted()) {
                Message message = getNextMessage_blocking();

                if (Thread.interrupted()) {
                    break;
                }

                if (message != null && obj != null) {
                    try {
                        method.invoke(obj, message);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        return new Thread(r);
    }

    public Thread subscribeAndCall_threadLoop(Handler handler) {
        Runnable r = () -> {
            while (!Thread.interrupted()) {
                Message message = getNextMessage_blocking();
                if (Thread.interrupted()) {
                    break;
                }
                if (message != null && handler != null) {
                    handler.handle(message);
                }
            }
        };
        return new Thread(r);
    }

    public void publish(String topic, String payload) {
        _client.publishWith().topic(topic).qos(MqttQos.AT_LEAST_ONCE).payload(payload.getBytes()).send();
    }

}
