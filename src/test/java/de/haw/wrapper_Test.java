package de.haw;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

public class wrapper_Test {
    List<String> topics = List.of("test/topic", "test/topic2", "test/topic3");
    private String topic = "test/topic";
    private String msg1 = "test";
    private String msg2 = "test2";
    private String msg3 = "test3";

    @Test
    public void testGetNextMessage_blocking() {
        Queue<Message> queue = new LinkedList<>();
        OwnMqttClient mqtt_ReceiverWrapper = new OwnMqttClient("hans");
        mqtt_ReceiverWrapper.createBlockingClient();
        mqtt_ReceiverWrapper.connectToBroker();
        mqtt_ReceiverWrapper.subscribe(topic);
        new Thread(() -> {
            queue.add(mqtt_ReceiverWrapper.getNextMessage_blocking());
        }).start();

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        OwnMqttClient mqtt_PublisherWrapper = new OwnMqttClient("bert");
        mqtt_PublisherWrapper.createBlockingClient();
        mqtt_PublisherWrapper.connectToBroker();
        mqtt_PublisherWrapper.publish(topic, msg1);

        while (queue.size() < 1) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        assertEquals(msg1, queue.poll().getPayload());

        mqtt_PublisherWrapper.disconnect();
        mqtt_ReceiverWrapper.disconnect();
    }

    @Test
    public void testSubscribeAndCall_threadLoop() {
        OwnMqttClient mqtt_ReceiverWrapper = new OwnMqttClient("hans2");
        mqtt_ReceiverWrapper.createBlockingClient();
        mqtt_ReceiverWrapper.connectToBroker();
        mqtt_ReceiverWrapper.subscribe(topic);

        TestHandler tH = new TestHandler(Arrays.asList(topic));
        Thread t = null;
        try {
            t = mqtt_ReceiverWrapper.subscribeAndCall_threadLoop(tH,
                    TestHandler.class.getDeclaredMethod("handle", Message.class));

            t.start();
        } catch (NoSuchMethodException e1) {
            e1.printStackTrace();
        } catch (SecurityException e1) {
            e1.printStackTrace();
        }

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        OwnMqttClient mqtt_PublisherWrapper = new OwnMqttClient("bert2");
        mqtt_PublisherWrapper.createBlockingClient();
        mqtt_PublisherWrapper.connectToBroker();
        mqtt_PublisherWrapper.publish(topic, msg1);

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(msg1, tH.getMessage().getPayload());

        if (tH != null)
            t.interrupt();
        mqtt_PublisherWrapper.disconnect();
        mqtt_ReceiverWrapper.disconnect();
    }

    @Test
    public void testSubscribeAndCall_threadLoop2() {
        OwnMqttClient mqtt_ReceiverWrapper = new OwnMqttClient("hans2");
        mqtt_ReceiverWrapper.createBlockingClient();
        mqtt_ReceiverWrapper.connectToBroker();
        mqtt_ReceiverWrapper.subscribe(topic);

        TestHandler tH = new TestHandler(Arrays.asList(topic));
        Thread t = null;
        try {
            t = mqtt_ReceiverWrapper.subscribeAndCall_threadLoop(tH,
                    TestHandler.class.getDeclaredMethod("handle", Message.class));

            t.start();
        } catch (NoSuchMethodException e1) {
            e1.printStackTrace();
        } catch (SecurityException e1) {
            e1.printStackTrace();
        }

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        OwnMqttClient mqtt_PublisherWrapper = new OwnMqttClient("bert2");
        mqtt_PublisherWrapper.createBlockingClient();
        mqtt_PublisherWrapper.connectToBroker();
        mqtt_PublisherWrapper.publish(topic, msg1);
        mqtt_PublisherWrapper.publish(topic, msg2);

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(msg1, tH.q.poll().getPayload());

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(msg2, tH.q.poll().getPayload());

        if (tH != null)
            t.interrupt();
        mqtt_PublisherWrapper.disconnect();
        mqtt_ReceiverWrapper.disconnect();
    }

    @Test
    public void testSubscribeAndCall_threadLoop_withMqttClientWrapper() {
        TestHandler tH = new TestHandler(topics);
        MqttClientWrapper mW = new MqttClientWrapper(tH, "client1");

        OwnMqttClient mqtt_PublisherWrapper = new OwnMqttClient("bert2");
        mqtt_PublisherWrapper.createBlockingClient();
        mqtt_PublisherWrapper.connectToBroker();
        mqtt_PublisherWrapper.publish(topics.get(1), msg1);
        mqtt_PublisherWrapper.publish(topics.get(2), msg2);

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(msg1, tH.q.poll().getPayload());

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        assertEquals(msg2, tH.q.poll().getPayload());

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mqtt_PublisherWrapper.publish(topics.get(1), msg3);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        assertEquals(msg3, tH.q.poll().getPayload());

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mqtt_PublisherWrapper.publish(topics.get(0), msg3);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(msg3, tH.q.poll().getPayload());
        mW.stop();
        mqtt_PublisherWrapper.disconnect();
    }
}
