package de.haw;


import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


public class TestHandler implements Handler{
    List<String> topics;
    Message message;
    Queue<Message> q = new LinkedList<>();
    MqttClientWrapper wrapper;


    public TestHandler(List<String> topics) {
        this.topics = topics;
    }


    public void handle(Message message) {
        System.out.println(message.toString());
        this.message = message;
        q.add(message);
    }

    public Message getMessage() {
        return this.message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public List<String> getTopics() {
        return topics;
    }


    @Override
    public void setMqttWrapper(MqttClientWrapper mqttWrapper) {
        this.wrapper = mqttWrapper;
    }


    @Override
    public void publish(Message message) {
        wrapper.publish(message); 
    }
}
