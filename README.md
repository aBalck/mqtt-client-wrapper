# mqtt-client-wrapper
## About
This is a simple wrapper for MQTT clients.

It allows:
- connect to a Mqtt broker
- publish messages
- and listen to specific topics

A 'handler' interface is given, which must be passed in the constructor of the wrapper.
The wrapper automatically creates a thread that calls the 'handle' method of the 'handler' implementation for each incoming message.

## How to use it
A class must be written that implements the 'Handler' interface.

Main:
```java
public static void main( String[] args )
    {
        Handler sH = new ServiceHandler();
        MqttClientWrapper mW = new MqttClientWrapper(sH, "chaosGen");
        sH.setMqttWrapper(mW);
    }
```

### Maven
1. Add your gitlab-access token to your settings.xml 
    - (unter Linux in: /ust/share/maven/conf)
```xml
<settings>
    <servers>
        <server>
        <id>gitlab-maven</id>
        <configuration>
            <httpHeaders>
            <property>
                <name>Private-Token</name>
                <value>REPLACE_WITH_YOUR_PERSONAL_ACCESS_TOKEN</value>
            </property>
            </httpHeaders>
        </configuration>
        </server>
    </servers>
</settings>
``` 

2. Add the dependecnie to your pom.xml
    - z.B.
 ```xml
    <dependency>
        <groupId>de.haw</groupId>
        <artifactId>cps-mqtt-client-wrapper</artifactId>
        <version>1.1-SNAPSHOT</version>
    </dependency>
```

3. Add the group repository
```xml
  <repositories>
    <repository>
      <id>gitlab-maven</id>
      <url>https://gitlab.com/api/v4/projects/37710978/packages/maven</url>
    </repository>
  </repositories>
```

4. Execute
```cmd
mvn dependency:get -Dartifact=de.haw:cps-mqtt-client-wrapper:1.1-SNAPSHOT
```

- [Choose the package you want and implement in your 'pom.xml'](https://git.haw-hamburg.de/cpsp-ws21-g2-team-balck-gedigk-sauer/cps-mqtt-client-wrapper/-/packages)
